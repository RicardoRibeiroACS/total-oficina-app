package br.com.acssistemas.totaloficina.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.acssistemas.totaloficina.R;
import br.com.acssistemas.totaloficina.activity.TaxCouponActivity;
import br.com.acssistemas.totaloficina.appClass.Historic;
import br.com.acssistemas.totaloficina.controler.HistoricAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoricFragment extends Fragment implements AdapterView.OnItemSelectedListener, HistoricAdapter.OnHistoricListener {

    private List<Historic> historicList = new ArrayList<>();
    private Spinner spinnerSelect;
    private RecyclerView recyclerView;
    private Context context;
    private TextView amount;

    public HistoricFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        this.context = context;
        super.onAttach(context);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_historic, container, false);

        spinnerSelect = view.findViewById(R.id.spinnerSelect);
        recyclerView = view.findViewById(R.id.recyclerViewHistoric);
        amount = view.findViewById(R.id.amount);

        HistoricAdapter historicAdapter = new HistoricAdapter(historicList, this);
        historicMaker();
        amountMaker();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(historicAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayout.VERTICAL));

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context, R.array.services, R.layout.support_simple_spinner_dropdown_item);
        spinnerSelect.setAdapter(adapter);
        spinnerSelect.setOnItemSelectedListener(this);

        return view;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void historicMaker() {

        Historic historic = new Historic("Revisão do veículo", "30/06/2019 às 12:05", "até KM 200000",
                "30/12/2019", "revisão geral", 150.00, 1);
        historicList.add(historic);

        historic = new Historic("Troca de lâmpadas da seta", "12/01/2019 às 9:05", "na garantia",
                "12/07/2019", "lampada X", 25.00, 2);
        historicList.add(historic);

        historic = new Historic("Troca de amortecedores dianteros", "20/03/2019 às 10:05", "na garantia",
                "20/03/2019", "cofap ar", 350.00, 2);
        historicList.add(historic);

        historic = new Historic("Troca de óleo de motor", "05/04/2019 às 11:05", "até KM 70000",
                "05/10/2019", "óleo Shell", 180.00, 1);
        historicList.add(historic);

        historic = new Historic("Troca de óleo de freio", "25/07/2019 às 12:05", "até KM 100000",
                "25/01/2020", "óleo de freio", 50.00, 1);
        historicList.add(historic);

        historic = new Historic("Troca de óleo da direção hidraulica", "25/07/2019 às 12:05", "até KM 100000",
                "25/01/2020", "óleo de freio", 50.00, 1);
        historicList.add(historic);
    }

    private void amountMaker() {
        amount.setText(historicList.size() + " registros localizados em seu histórico");
    }

    @Override
    public void onHistoricClick(int position) {
        Intent intent = new Intent(HistoricFragment.super.getContext(), TaxCouponActivity.class);
        Historic historic = historicList.get(position);
        intent.putExtra("object", historic);
        startActivity(intent);
    }
}
