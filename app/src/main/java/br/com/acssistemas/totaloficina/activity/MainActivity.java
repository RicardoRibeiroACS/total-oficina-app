package br.com.acssistemas.totaloficina.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.Menu;

import br.com.acssistemas.totaloficina.R;
import br.com.acssistemas.totaloficina.fragments.CarsFragment;
import br.com.acssistemas.totaloficina.fragments.CatalogFragment;
import br.com.acssistemas.totaloficina.fragments.HelpFragment;
import br.com.acssistemas.totaloficina.fragments.HistoricFragment;
import br.com.acssistemas.totaloficina.fragments.HomeFragment;
import br.com.acssistemas.totaloficina.fragments.WorkshopFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(0).setChecked(true);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        HomeFragment home = new HomeFragment();
        fragmentChange(home);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            HomeFragment home = new HomeFragment();
            fragmentChange(home);
        } else if (id == R.id.nav_catalog) {
            CatalogFragment catalogFragment = new CatalogFragment();
            fragmentChange(catalogFragment);
        } else if (id == R.id.nav_historic) {
            HistoricFragment historicFragment = new HistoricFragment();
            fragmentChange(historicFragment);
        } else if (id == R.id.nav_workshop) {
            WorkshopFragment workshopFragment = new WorkshopFragment();
            fragmentChange(workshopFragment);
        } else if (id == R.id.nav_cars) {
            CarsFragment carsFragment = new CarsFragment();
            fragmentChange(carsFragment);
        } else if (id == R.id.nav_help) {
            HelpFragment helpFragment = new HelpFragment();
            fragmentChange(helpFragment);
        } else if (id == R.id.nav_facebook) {
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/1016670125075667")));
            } catch(Exception e) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/sistemaacs")));
            }
        } else if (id == R.id.nav_instagram) {
//            try {
//
//            }catch (Exception e) {
//
//            }
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void fragmentChange(Fragment object) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frameLayout, object);
        transaction.commit();
    }
}
