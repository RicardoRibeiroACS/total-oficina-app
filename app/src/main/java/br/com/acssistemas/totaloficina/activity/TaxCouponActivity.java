package br.com.acssistemas.totaloficina.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

import br.com.acssistemas.totaloficina.R;
import br.com.acssistemas.totaloficina.appClass.Historic;

public class TaxCouponActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private Historic historic;
    private TextView description, status, date, warrantyDate, product, unitaryValue, productAmount,
            totalUnitaryValue, partialValue, totalValue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tax_coupon);


        if (getIntent().hasExtra("object")) {
            historic = getIntent().getParcelableExtra("object");
        }

        toolbar = findViewById(R.id.toolbarTaxCoupon);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");

        description = findViewById(R.id.description);
        status = findViewById(R.id.status);
        date = findViewById(R.id.date);
        warrantyDate = findViewById(R.id.warrantyDate);
        product = findViewById(R.id.product);
        unitaryValue = findViewById(R.id.unitaryValue);
        productAmount = findViewById(R.id.productAmount);
        totalUnitaryValue = findViewById(R.id.totalUnitaryValue);
        partialValue = findViewById(R.id.partialValue);
        totalValue = findViewById(R.id.totalValue);

        description.setText(historic.getDescription());

        if (historic.getStatus().equals("na garantia")) {
            status.setBackgroundColor(Color.parseColor("#2ECC71"));
        } else {
            status.setBackgroundColor(Color.parseColor("#3498DB"));
        }

        DecimalFormat df = new DecimalFormat("'R$' #,##0.00");

        status.setText(historic.getStatus());
        date.setText(historic.getDate());
        warrantyDate.setText("Garantia válida até " + historic.getWarrantyDate());
        product.setText(historic.getProduct());
        unitaryValue.setText(df.format(historic.getUnitaryValue()));
        productAmount.setText(String.valueOf(historic.getProductAmount()));
        totalUnitaryValue.setText(df.format(totalUnitaryValueFactory()));
        partialValue.setText(df.format(totalPartialValueFactory()));
        totalValue.setText(df.format(totalPartialValueFactory()));
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if(id == android.R.id.home){
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @NonNull
    private Double totalUnitaryValueFactory() {

        return (historic.getProductAmount() * historic.getUnitaryValue());
    }

    @NonNull
    private Double totalPartialValueFactory() {
        Double totalUnitary = totalUnitaryValueFactory();

        return totalUnitary + 50.00;
    }

    public void informButton(View view) {
        Toast.makeText(getApplicationContext(), "Fotos da excução do serviço", Toast.LENGTH_LONG).show();
    }
}
