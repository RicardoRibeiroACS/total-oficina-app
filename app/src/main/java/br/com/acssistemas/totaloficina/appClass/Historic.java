package br.com.acssistemas.totaloficina.appClass;


import android.os.Parcel;
import android.os.Parcelable;

public class Historic implements Parcelable {
   private String description;
   private String date;
   private String status;
   private String warrantyDate;
   private String product;
   private Double unitaryValue;
   private int productAmount;

    public Historic(String description, String date, String status, String warrantyDate,
                    String product, Double unitaryValue, int productAmount) {
        this.description = description;
        this.date = date;
        this.status = status;
        this.warrantyDate = warrantyDate;
        this.product = product;
        this.unitaryValue = unitaryValue;
        this.productAmount = productAmount;
    }

    public Historic() {
    }

    protected Historic(Parcel in) {
        description = in.readString();
        date = in.readString();
        status = in.readString();
        warrantyDate = in.readString();
        product = in.readString();
        if (in.readByte() == 0) {
            unitaryValue = null;
        } else {
            unitaryValue = in.readDouble();
        }
        productAmount = in.readInt();
    }

    public static final Creator<Historic> CREATOR = new Creator<Historic>() {
        @Override
        public Historic createFromParcel(Parcel in) {
            return new Historic(in);
        }

        @Override
        public Historic[] newArray(int size) {
            return new Historic[size];
        }
    };

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getWarrantyDate() {
        return warrantyDate;
    }

    public void setWarrantyDate(String warrantyDate) {
        this.warrantyDate = warrantyDate;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public Double getUnitaryValue() {
        return unitaryValue;
    }

    public void setUnitaryValue(Double unitaryValue) {
        this.unitaryValue = unitaryValue;
    }

    public int getProductAmount() {
        return productAmount;
    }

    public void setProductAmount(int productAmount) {
        this.productAmount = productAmount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(description);
        parcel.writeString(date);
        parcel.writeString(status);
        parcel.writeString(warrantyDate);
        parcel.writeString(product);
        if (unitaryValue == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeDouble(unitaryValue);
        }
        parcel.writeInt(productAmount);
    }
}
