package br.com.acssistemas.totaloficina.appClass;

public class Services {

    private String serviceName;

    public Services() {

    }

    public Services(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
}
