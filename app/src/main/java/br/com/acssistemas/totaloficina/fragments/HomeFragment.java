package br.com.acssistemas.totaloficina.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ViewListener;

import br.com.acssistemas.totaloficina.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private CarouselView carouselView;
    private Integer [] images = {R.drawable.slider1, R.drawable.slider2, R.drawable.slider3};

    public HomeFragment() {
        // Required empty public constructor
    }

//    private ViewPager viewPager;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);
        carouselView = view.findViewById(R.id.carouselView);
        carouselView.setPageCount(images.length);
        carouselView.setViewListener(viewListener);

//        viewPager = view.findViewById(R.id.viewPager);
//        SliderViewPagerAdapter adapter = new SliderViewPagerAdapter(getActivity());
//        viewPager.setAdapter(adapter);

        return view;
    }
    ViewListener viewListener = new ViewListener() {
        @Override
        public View setViewForPosition(int position) {
            View customView = getLayoutInflater().inflate(R.layout.slider_layout, null);
            ImageView imageView = customView.findViewById(R.id.imageViewSlider);
            imageView.setImageResource(images[position]);

            return customView;
        }
    };
}
