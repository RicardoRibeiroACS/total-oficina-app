package br.com.acssistemas.totaloficina.fragments;


import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import br.com.acssistemas.totaloficina.R;
import br.com.acssistemas.totaloficina.appClass.Services;
import br.com.acssistemas.totaloficina.controler.ServicesAdapter;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 */
public class CatalogFragment extends Fragment implements View.OnClickListener {


    private List<Services> servicesList = new ArrayList<>();
    private RecyclerView recyclerView;
    private Context context;
    private Button searchButton;
    private EditText inputSearch;


    public CatalogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        this.context = context;
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_catalog, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        searchButton = view.findViewById(R.id.searchButton);
        inputSearch = view.findViewById(R.id.inputSearch);

        searchButton.setOnClickListener(this);

        ServicesAdapter adapter = new ServicesAdapter(servicesList);
        serviceMaker();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayout.VERTICAL));
        return view;
    }

    public void serviceMaker() {
        Services service = new Services("Troca de correia dentada");
        servicesList.add(service);

        service = new Services("Troca de pastilha do freio");
        servicesList.add(service);

        service = new Services("Troca de lâmpada do farol");
        servicesList.add(service);

        service = new Services("Troca de lâmpada da seta");
        servicesList.add(service);

        service = new Services("Troca de óleo de câmbio");
        servicesList.add(service);

        service = new Services("Troca de óleo de motor");
        servicesList.add(service);

        service = new Services("Troca de pastilha do freio");
        servicesList.add(service);

        service = new Services("Troca de lâmpada do farol");
        servicesList.add(service);

        service = new Services("Troca de lâmpada da seta");
        servicesList.add(service);

        service = new Services("Troca de óleo de câmbio");
        servicesList.add(service);

        service = new Services("Troca de óleo de motor");
        servicesList.add(service);
    }

    @Override
    public void onClick(View view) {
        InputMethodManager inputManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }
}
