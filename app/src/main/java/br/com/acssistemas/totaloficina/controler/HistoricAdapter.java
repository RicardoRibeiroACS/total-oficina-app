package br.com.acssistemas.totaloficina.controler;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.acssistemas.totaloficina.R;
import br.com.acssistemas.totaloficina.appClass.Historic;

public class HistoricAdapter extends RecyclerView.Adapter<HistoricAdapter.MyViewHolder> {

    private List<Historic> historicList;
    private OnHistoricListener onHistoricListener;

    public HistoricAdapter(List<Historic> historicList,  OnHistoricListener onHistoricListener) {
        this.historicList = historicList;
        this.onHistoricListener = onHistoricListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.historic_list, parent, false);

        return new MyViewHolder(view, onHistoricListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Historic historic = historicList.get(position);
        holder.description.setText(historic.getDescription());
        holder.date.setText(historic.getDate());
        holder.status.setText(historic.getStatus());

        if (historic.getStatus().equals("na garantia")) {
            holder.status.setBackgroundColor(Color.parseColor("#2ECC71"));
        } else {
            holder.status.setBackgroundColor(Color.parseColor("#3498DB"));
        }
        holder.status.setTextColor(Color.WHITE);
    }

    @Override
    public int getItemCount() {
        return historicList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView description, date, status;
        ImageButton more;
        OnHistoricListener onHistoricListener;

        public MyViewHolder(@NonNull View itemView, OnHistoricListener onHistoricListener) {
            super(itemView);

            description = itemView.findViewById(R.id.description);
            date = itemView.findViewById(R.id.date);
            status = itemView.findViewById(R.id.status);
            more = itemView.findViewById(R.id.more);
            this.onHistoricListener = onHistoricListener;

            more.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onHistoricListener.onHistoricClick(getAdapterPosition());
        }
    }

    public interface OnHistoricListener {
        void onHistoricClick(int position);
    }
}